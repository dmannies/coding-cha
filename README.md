# Code Challenge #

This repository contains my response to the coding challenge defined 
[here](https://gist.github.com/mikedelorenzo-koneksa/3d273f862e919782a181c28ef5f50f54).

My response includes the following implementations:

* Typescript [docs](https://altrealtech.com/dmannies/coding-challenge/typescript/compodoc/index.html)
* Angular 11 [docs](https://altrealtech.com/dmannies/coding-challenge/angular11/compodoc/index.html) [demo](https://altrealtech.com/dmannies/coding-challenge/angular11/index.html)

The Typescript version is found in the typescript 
directory. The typescript version can be invoked 
from the command line in the typescirpt directory:

* "npm run start" with 1 argument of a relative filename like 
  "src/PizzaDeliveryInput.txt".

npm run start -- 'src/PizzaDeliveryInput.txt'

read 8193 characters from src/PizzaDeliveryInput.txt
Part 1:
Delivered 8193 pizzas to 2565 houses using 1 deliveree.
Part 2:
Delivered 8194 pizzas to 2639 houses using 2 deliverees.

* "npm run start" with 2 arguments of an integer 1-4 of deliveryees 
  and a string of dispatch commands. 

npm run start -- 1 '>^>^>'

read 1 count and 5 characters from command line
Delivered 6 pizzas to 6 houses using 1 deliverees.

* "npm run start:all".

While run the first method with the input file.

The Angular11 version is found in the angular11
directory. A live demo can be found [here]().

